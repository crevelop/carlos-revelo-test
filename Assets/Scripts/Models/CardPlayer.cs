﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardPlayer
{
    [JsonDeserialize("Name")]
    public string Name;
    [JsonDeserialize("Velocity")]
    public int Velocity;
    [JsonDeserialize("Color")]
    public string CarColor;
    [JsonDeserialize("Icon")]
    public string IconUrl;
}
