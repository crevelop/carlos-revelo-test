﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CardRace
{
    [JsonDeserialize("GameConfiguration")]
    public CardConfiguration Configuration;
    [JsonDeserialize("Players")]
    public List<CardPlayer> Players = new List<CardPlayer>();

    public static CardRace LoadData()
    {
        CardRace raceData = null;
        string path = Application.streamingAssetsPath + "/Data.txt";
        var data = File.ReadAllText(path);
        if (data != null)
        {
            raceData = JsonHelper.Deserialize<CardRace>(data);
            Debug.Log("Race data loaded succesfully");
        }
        else
            Debug.LogError("There was an error reading data file from path: " + path);

        return raceData;
    }
}
