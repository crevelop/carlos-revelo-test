﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardConfiguration
{
    [JsonDeserialize("lapsNumber")]
    public int Laps;
    [JsonDeserialize("playersInstantiationDelay")]
    public int InstatiationDelay;
}
