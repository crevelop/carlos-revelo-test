﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This singleton is the main point of entry for object interaction.
/// Races are loaded and started from here
/// </summary>

public class RaceManager : MonoSingleton<RaceManager>
{
    [SerializeField]
    Car carPrefab;

    public PathManager Path;
    public CardRace Data;
    public List<Car> Cars = new List<Car>();

    float rankingUpdateTimer;

    protected override void Initialize()
    {
        //Update Nodes data
        Path.UpdateNodes();
        //Load Players data from Json
        Data = CardRace.LoadData();
        RaceData.Instance.LapsToWin = Data.Configuration.Laps;
        RaceData.Instance.WayPointsPerLap = Path.Nodes.Count;
    }

    public void LoadNewRace()
    {
        foreach (Car car in Cars)
            car.RemoveFromRace();

        RaceData.Instance.Reset();
    }

    public void StartRace()
    {
        RaceData.Instance.State.Value = RaceState.Racing;
        RaceData.Instance.Players = ChooseRandomPlayers(8);
        LoadPlayers();
    }

    void Update()
    {
        var race = RaceData.Instance;
        if (race.State.Value == RaceState.Racing)
        {
            //Rank players based on Progress - avoid every frame for performance
            rankingUpdateTimer += Time.deltaTime;
            if (rankingUpdateTimer > 0.5f)
            {
                rankingUpdateTimer = 0;
                RaceData.Instance.UpdateRank();
            }
        }
    }

    List<CardPlayer> ChooseRandomPlayers(int numberOfPlayers)
    {
        Dictionary<int, CardPlayer> selected = new Dictionary<int, CardPlayer>();
        List<CardPlayer> random = new List<CardPlayer>();
        int k = Mathf.Min(Data.Players.Count - 1, numberOfPlayers);

        while (selected.Count < k)
        {
            int index = Random.Range(0, Data.Players.Count);
            if (!selected.ContainsKey(index))
                selected.Add(index, Data.Players[index]);
        }

        var selectedPlayers = new List<CardPlayer>(selected.Values);
        selectedPlayers.Sort((a, b) => a.Velocity.CompareTo(b.Velocity));
        return selectedPlayers;
    }

    public void LoadPlayers()
    {
        StopAllCoroutines();
        StartCoroutine(LoadPlayersWorker());
    }

    //Load Players for new race
    IEnumerator LoadPlayersWorker()
    {
        foreach (CardPlayer player in RaceData.Instance.Players)
        {
            Car car = null;
            var availableCar = Cars.Find(el => !el.IsRacing);
            if (availableCar != null)
                car = availableCar;
            else
            {
                car = Instantiate<Car>(carPrefab, transform);
                Cars.Add(car);
            }
            car.AddToRace(player);
            yield return new WaitForSeconds(Data.Configuration.InstatiationDelay * 0.001f);
        }
    }
}
