﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This singleton holds the current race data.
/// Data is updated and subscribers are informed when changes are applied. See Property class
/// </summary>

public class RaceData
{
    public static RaceData Instance = new RaceData();
    public List<CardPlayer> Players = new List<CardPlayer>();
    public Property<RaceState> State = new Property<RaceState>(RaceState.WarmingUp);
    public List<Car> Ranking = new List<Car>();
    public Property<List<Car>> OnRankingChanged = new Property<List<Car>>(new List<Car>());
    public Property<Car> OnCarJoined = new Property<Car>();
    public int LapsToWin;
    public int WayPointsPerLap;

    public void Reset()
    {
        State.Value = RaceState.WarmingUp;
        Players = new List<CardPlayer>();
        Ranking = new List<Car>();
    }

    public void UpdateRank()
    {
        Ranking.Sort((a, b) => b.Ranking.Progress.CompareTo(a.Ranking.Progress));
        OnRankingChanged.Fire(Ranking);
        if (Ranking.Count > 0 && Ranking[0].Ranking.LapCount >= LapsToWin)
            State.Value = RaceState.Finished;
    }
}

public enum RaceState
{
    WarmingUp,
    Racing,
    Finished
}