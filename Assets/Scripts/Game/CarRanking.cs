﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarRanking
{
    public int Progress { get; private set; }
    public int LapCount { get; set; }
    public int WayPoint { get; set; }
    public float TimeElapsed { get; set; }

    public float Completion
    {
        get
        {
            var waypointsReached = RaceData.Instance.WayPointsPerLap * LapCount + WayPoint;
            var totalWaypointsInRace = RaceData.Instance.WayPointsPerLap * RaceData.Instance.LapsToWin;
            return (float)(waypointsReached) / (float)(totalWaypointsInRace);
        }
    }
    
    public void UpdateRanking(Car car)
    {
        //Evaluate ranking based on number of laps, waypoints and distance from last waypoint.
        float distanceFromLastNode = Vector3.Distance(car.CachedTransform.position, car.LastNode.CachedTransform.position);
        Progress = LapCount * 10000 + WayPoint * 100 + (int)distanceFromLastNode;
    }
}
