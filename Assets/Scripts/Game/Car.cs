﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    [SerializeField]
    MeshRenderer bodyMesh;

    Rigidbody rb;
    public CardPlayer Player { get; private set; }
    public Node.Lane CurrenLane = Node.Lane.Middle;
    public Transform CachedTransform { get; private set; }
    public Node NextNode { get; private set; }
    public Node LastNode { get; private set; }
    public bool IsRacing { get; private set; }
    public CarRanking Ranking { get; private set; }

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        CachedTransform = transform;
        Reset();
    }

    public void AddToRace(CardPlayer player)
    {
        gameObject.SetActive(true);
        Reset();
        Player = player;
        IsRacing = true;
        RaceData.Instance.Ranking.Add(this);
        bodyMesh.material.color = Utils.hexToColor(player.CarColor);
        RaceData.Instance.OnCarJoined.Fire(this);
    }

    public void RemoveFromRace()
    {
        gameObject.SetActive(false);
        IsRacing = false;
    }

    void Reset()
    {
        Ranking = new CarRanking();
        LastNode = RaceManager.Instance.Path.Nodes[0];
        NextNode = RaceManager.Instance.Path.Nodes[1];
        CachedTransform.position = LastNode.CachedTransform.position;
        CachedTransform.rotation = Quaternion.identity;
        CurrenLane = Node.Lane.Middle;
    }

    void Update()
    {
        if(Ranking.LapCount < 4)
            Ranking.TimeElapsed += Time.deltaTime;
    }

    void FixedUpdate()
    {
        //Check if car is not flying or colliding against other objects
        if (rb.angularVelocity.sqrMagnitude > 1f)
            return;

        //Check if other car is in front
        RaycastHit hit;
        if (Physics.SphereCast(CachedTransform.position, 5, CachedTransform.forward, out hit, 20))
        {
            if (hit.collider.CompareTag("Car"))
            {
                //If car belongs to same lane, switch lane
                Node.Lane otherCarLane = hit.collider.GetComponentInParent<Car>().CurrenLane;
                if (otherCarLane == CurrenLane)
                {
                    switch(CurrenLane)
                    {
                        case Node.Lane.Left: CurrenLane = Node.Lane.Middle; break;
                        case Node.Lane.Middle: CurrenLane = (Random.value > 0.5f? Node.Lane.Right : Node.Lane.Left); break;
                        case Node.Lane.Right: CurrenLane = Node.Lane.Middle; break;
                    }
                }
            }
        }

        //Go to the next node in path
        Vector3 targetDirection = (NextNode.Lanes[CurrenLane] - CachedTransform.position);
        rb.velocity = targetDirection.normalized * Player.Velocity;
        Quaternion nextLook = Quaternion.LookRotation(targetDirection);
        CachedTransform.rotation = Quaternion.Slerp(CachedTransform.rotation, nextLook, Time.deltaTime * 5f);

        //Check if next node reached
        if (targetDirection.sqrMagnitude < 4f)
        {
            //Did we complete a Lap?
            if (NextNode.Index == 0)
                Ranking.LapCount++;

            //Set target to next node and store previous
            var nodes = RaceManager.Instance.Path.Nodes;
            int next = NextNode.Index + 1;
            next = next > nodes.Count - 1 ? 0 : next;
            LastNode = NextNode;
            NextNode = nodes[next];
            Ranking.WayPoint = LastNode.Index;
        }

        Ranking.UpdateRanking(this);
    }
}
