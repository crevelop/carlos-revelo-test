﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFinishedPlayer : MonoBehaviour
{
    [SerializeField]
    Text playerName;
    [SerializeField]
    RawImage icon;
    [SerializeField]
    Outline playerColor;

    IconContainer iconContainer = null;

    public void Set(CardPlayer player)
    {
        playerName.text = player.Name;
        playerColor.effectColor = Utils.hexToColor(player.CarColor);
        iconContainer = IconManager.Instance.GetIcon(player.IconUrl);
    }

    void Update()
    {
        if (iconContainer != null && iconContainer.IsReady)
            icon.texture = iconContainer.Icon;
    }
}
