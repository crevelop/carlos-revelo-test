﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICarMarkers : MonoBehaviour
{
    [SerializeField]
    UICarMarkerItem markerTemplate;

    List<UICarMarkerItem> items = new List<UICarMarkerItem>();

    void OnEnable()
    {
        foreach (UICarMarkerItem item in items)
            item.gameObject.SetActive(false);
    }

    void Start()
    {
        markerTemplate.gameObject.SetActive(false);

        RaceData.Instance.OnCarJoined.AddEventAndFire(newCar =>
        {
            if (newCar != null)
                AddMarker(newCar);
        }, this);
    }

    void AddMarker(Car newCar)
    {
        UICarMarkerItem item = items.Find(el => !el.gameObject.activeInHierarchy);
        if (item == null)
        {
            item = Instantiate(markerTemplate, transform);
            items.Add(item);
        }
        item.gameObject.SetActive(true);
        item.Set(newCar);
    }
}


