﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRaceState : MonoBehaviour
{
    [SerializeField]
    GameObject uiWarmingUp;
    [SerializeField]
    GameObject uiRacing;
    [SerializeField]
    GameObject uiFinished;

    void Start()
    {
        RaceData.Instance.State.AddEventAndFire(state=>
        {
            uiWarmingUp.SetActive(state == RaceState.WarmingUp);
            uiRacing.SetActive(state == RaceState.Racing);
            uiFinished.SetActive(state == RaceState.Finished);
        }, this);
    }
}