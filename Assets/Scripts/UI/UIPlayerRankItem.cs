﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPlayerRankItem : MonoBehaviour
{
    [SerializeField]
    Text playerName;
    [SerializeField]
    Outline playerColor;
    [SerializeField]
    Image progress;
    [SerializeField]
    RawImage icon;

    IconContainer iconContainer = null;

    public void Set(Car car)
    {
        playerName.text = car.Player.Name;
        playerColor.effectColor = Utils.hexToColor(car.Player.CarColor);
        progress.fillAmount = Mathf.Clamp01(car.Ranking.Completion);
        iconContainer = IconManager.Instance.GetIcon(car.Player.IconUrl);
    }

    void Update()
    {
        if (iconContainer != null && iconContainer.IsReady)
            icon.texture = iconContainer.Icon;
    }
}
