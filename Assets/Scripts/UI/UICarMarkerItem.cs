﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICarMarkerItem : MonoBehaviour
{
    [SerializeField]
    Text playerName;

    Car currrentCar;
    Transform cachedTransform;
    float targetDistance = 0;

    void Awake()
    {
        cachedTransform = transform;
    }

    public  void Set(Car newCar)
    {
        playerName.text = newCar.Player.Name;
        playerName.color = Utils.hexToColor(newCar.Player.CarColor);
        currrentCar = newCar;
    }

    void Update()
    {
        if (currrentCar == null) return;

        var camera = Camera.main;
        targetDistance = Vector3.Distance(camera.transform.position, currrentCar.CachedTransform.position);
        var yOffset = Utils.Remap(targetDistance, 10f, 30f, 1.8f, 3f);
        cachedTransform.position = camera.WorldToScreenPoint(currrentCar.CachedTransform.position);
    }

}
