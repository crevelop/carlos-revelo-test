﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerRank : MonoBehaviour
{
    [SerializeField]
    UIPlayerRankItem rankItemTemplate;

    List<UIPlayerRankItem> items = new List<UIPlayerRankItem>();

    void OnEnable()
    {
        foreach (UIPlayerRankItem item in items)
            item.gameObject.SetActive(false);
    }

    void Start()
    {
        rankItemTemplate.gameObject.SetActive(false);

        RaceData.Instance.OnRankingChanged.AddEventAndFire(ranking =>
        {
            for (int i = 0; i < ranking.Count && i < 4; i++)
                SetPosition(i, ranking);
        }, this);
    }

    void SetPosition(int position, List<Car> source)
    {
        UIPlayerRankItem item = null;
        if (items.Count >= position + 1)
        {
            item = items[position];
        }
        else
        {
            item = Instantiate(rankItemTemplate, transform);
            items.Add(item);
        }
        item.gameObject.SetActive(true);
        item.Set(source[position]);
    }
}
