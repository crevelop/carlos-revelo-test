﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconManager : MonoSingleton<IconManager>
{
    Dictionary<string, IconContainer> icons = new Dictionary<string, IconContainer>();

    public IconContainer GetIcon(string iconUrl)
    {
        IconContainer container = new IconContainer();
        if (icons.ContainsKey(iconUrl))
            container = icons[iconUrl];
        else
        {
            icons.Add(iconUrl, container);
            StartCoroutine(LoadIcon(iconUrl, container));
        }
        return container;
    }

    IEnumerator LoadIcon(string url, IconContainer temp)
    {
        WWW www = new WWW(url);
        yield return www;
        temp.Icon = www.texture;
        temp.IsReady = true;
    }
}

public class IconContainer
{
    public Texture2D Icon;
    public bool IsReady;
}
