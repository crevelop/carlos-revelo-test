﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFinished : MonoBehaviour
{
    [SerializeField]
    Text winnerTitle;
    [SerializeField]
    UIFinishedPlayer first;
    [SerializeField]
    UIFinishedPlayer second;
    [SerializeField]
    UIFinishedPlayer third;
    [SerializeField]
    Button closeButton;
    [SerializeField]
    AudioSource soundFX;

    void OnEnable()
    {
        soundFX.Play();
    }

    void Start()
    {
        RaceData.Instance.State.AddEventAndFire(state =>
        {
            if(state == RaceState.Finished)
            {
                var ranking = RaceData.Instance.Ranking;

                winnerTitle.text = string.Format("{0} WINS", ranking[0].Player.Name.ToUpper());
                first.Set(ranking[0].Player);
                second.Set(ranking[1].Player);
                third.Set(ranking[2].Player);
            }
        }, this);

        closeButton.onClick.AddListener(() => RaceManager.Instance.LoadNewRace());
    }
}
