﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIWarmingUp : MonoBehaviour
{
    [SerializeField]
    Button startButton;
    [SerializeField]
    Text countdown;
    [SerializeField]
    AudioSource soundFX;

    void OnEnable()
    {
        startButton.gameObject.SetActive(true);
        countdown.gameObject.SetActive(false);
    }

    void Start()
    {
        startButton.onClick.AddListener(() =>
        {
            StopAllCoroutines();
            StartCoroutine(Countdown());
        });
    }

    IEnumerator Countdown()
    {
        startButton.gameObject.SetActive(false);
        countdown.gameObject.SetActive(true);
        soundFX.Play();
        countdown.color = Color.red;
        countdown.text = "3";
        yield return new WaitForSeconds(1f);
        countdown.color = Color.red;
        countdown.text = "2";
        yield return new WaitForSeconds(1f);
        countdown.color = Color.yellow;
        countdown.text = "1";
        yield return new WaitForSeconds(1f);
        countdown.color = Color.green;
        countdown.text = "GO!";
        yield return new WaitForSeconds(1f);
        RaceManager.Instance.StartRace();
    }
}
