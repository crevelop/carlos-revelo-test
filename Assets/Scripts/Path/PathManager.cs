﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathManager : MonoBehaviour
{
    [SerializeField]
    Color nodeColor;
    [SerializeField]
    Color lineColor;

    public GameObject NodePrefab;
    public List<Node> Nodes { get { return nodes; } }

    List<Node> nodes = new List<Node>();

    public void UpdateNodes()
    {
        Node[] children = GetComponentsInChildren<Node>();
        nodes = new List<Node>();

        for (int i = 0; i < children.Length; i++)
        {
            var child = children[i];
            var previous = i > 0? children[i - 1]: null;
            if (child != transform)
            {
                child.Set(i);
                nodes.Add(child);
                if(previous != null)
                    previous.transform.rotation = Quaternion.LookRotation(child.transform.position - previous.transform.position);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        UpdateNodes();
        for (int i = 0; i < nodes.Count; i++)
        {
            Vector3 currentNode = nodes[i].transform.position;
            Vector3 previousNode = Vector3.zero;

            if (i > 0)
                previousNode = nodes[i - 1].transform.position;
            else if (i == 0 && nodes.Count > 1)
                previousNode = nodes[nodes.Count - 1].transform.position;
            
            Gizmos.color = lineColor;
            Gizmos.DrawLine(previousNode, currentNode);

            Gizmos.color = nodeColor;
            Gizmos.DrawWireSphere(currentNode, 1f);
        }
    }

}
