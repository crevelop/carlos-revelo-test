﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public enum Lane
    {
        Middle,
        Left,
        Right
    }

    [SerializeField]
    float laneWidth = 16f;

    public Transform CachedTransform { get; private set; }
    public int Index { get; private set; }
    public Dictionary<Lane, Vector3> Lanes = new Dictionary<Lane, Vector3>();

    void Awake()
    {
        CachedTransform = transform;
        Lanes.Add(Lane.Middle, CachedTransform.position);
        Lanes.Add(Lane.Left, CachedTransform.position - CachedTransform.right * (laneWidth * 0.5f));
        Lanes.Add(Lane.Right, CachedTransform.position + CachedTransform.right * (laneWidth * 0.5f));
    }

    public void Set(int index)
    {
        name = "Node-" + index;
        Index = index;
    }

    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 2f);

        Vector3 left = transform.position - transform.right * (laneWidth * 0.5f);
        Vector3 right = transform.position + transform.right * (laneWidth * 0.5f);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(transform.position, left);
        Gizmos.DrawLine(transform.position, right);

        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(left, 1f);
        Gizmos.DrawWireSphere(right, 1f);
    }
}
