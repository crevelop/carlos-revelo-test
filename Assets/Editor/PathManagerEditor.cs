﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(PathManager))]
public class PathManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();

        PathManager path = (PathManager)target;

        GUILayout.Space(20f);
        if (GUILayout.Button("Add Node"))
        {
            Vector3 pos = Vector3.zero;
            if (path.Nodes.Count > 0)
                pos = path.Nodes[path.Nodes.Count - 1].transform.position;

            GameObject go = Instantiate(path.NodePrefab, pos, Quaternion.identity, path.transform);
            Selection.activeGameObject = go;
        }
        GUILayout.Space(50f);

        GUILayout.BeginVertical();
        foreach (Node node in path.Nodes)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label(node.name);
            if (GUILayout.Button("Select"))
            {
                Selection.activeGameObject = node.gameObject;
            }
            if (GUILayout.Button("Insert"))
            {
                GameObject go = Instantiate(path.NodePrefab, node.transform.position, Quaternion.identity, path.transform);
                go.transform.SetSiblingIndex(node.transform.GetSiblingIndex() + 1);
                Selection.activeGameObject = go;
            }
            if (GUILayout.Button("Remove"))
            {
                DestroyImmediate(node.gameObject);
            }
            GUILayout.EndHorizontal();
            GUILayout.Space(10f);
        }

        GUILayout.EndVertical();
        GUILayout.Space(50f);
    }
}
